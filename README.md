# Garrod Seminar series Michaelmas term 2023

A small app to explore the producers and resellers of products used at Garrod Seminar's receptions.

## Shiny App

To run it :

```bash
/usr/bin/Rscript -e "shiny::runApp('.')"
```


## QRCODES

To generate the QR code, with debian one can use qrencode:

```
qrencode localhost:5715:?city=London
```

This will create a qrcode for the url `localhost:5715:?city=London`

A more flexible way to more easily use the info store in the list of producter in the R script is to used R using the script `generateQrcode.R` based on the library [qrencoder](https://cran.r-project.org/web/packages/qrencoder/index.html).

```bash
Rscript generateQrcode.R #generate qrcode for all products
pdftk qr_*.pdf cat output multiqr.pdf #concatenate l qrcode in onde pdf
pdfbook2 multiqr.pdf  #create a book to reduce to 2 QR code per page
```

