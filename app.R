library(shiny)
library(leaflet)
library(htmlwidgets)


# Define UI
ui <- fluidPage(
				titlePanel("Empirical Network of Cultural Transmission"),
                br(),
                br(),
				fluidRow(
						 column(4,
								uiOutput("infos"),br()
								),
						 column(8,
                                br(),
								leafletOutput("mymap", width = "80%",height="85vh")
								)

				)
)

allcolab=list(
              stir=list(
                        name="Stir",
                        address= "Green Street, Cambridge",
                        product="Surdough Bread",
                        description="Stir is a Bakery, Café in Cambridge, providing a handmade, locally sourced bread.",
                        image="stir.jpg",
                        website="https://stircambridge.co.uk/"
              ),
              past=list(
                        name=" Pastim Artisan Bakery",
                        address= "Butt Ln, Milton",
                        product="Artisan Surdough, Bread",
                        description="Pastim is a small family run bakery situed in Milton, Cambridge with a generational bakers background. They came in Cambridge a few years ago, from the Valencian region in Spain. They are specialists in sourdough bread and natural processes for their products, which is their signature of quality.",
                        image="pastim.jpg",
                        website=""
                        ),
              ogle=list(
                        name="Ogleshield",
                        address="North Cadbury, Somerset, England",
                        product="Ogleshield Cheese",
                        description="A raclette style cheese from Jamie Montgomery",
                        image="ogle.jpg",
                        website=""
                        ),
              comte=list(
                         name="Fromagerie Seignemartin",
                         address="Natua, Ain",
                         product="8 month old, Comte Cheese",
                         description="The Seignemartin cheese dairy selects their cheese wheels themselves from various producers in the Jura region and mature them following a careful process developped since the beginning of the factory in 1930. This Comte has been matured during 8 month but, as Stéphane, the cheesemonger from L'Échanson who sold me the cheese told me: don't be fooled by how long the cheese has been aged; it's all about how it has been done! The temperature, humidity, and care given to maturation are what make all the difference. Stéphane explained to me that when he visited the cave of Seignmartin, he was amazed at how two wheels, aged for the same amount of time but in slightly different conditions, could have different tastes and textures.",
                         image="comte.jpg",
                         website="http://www.comte-seignemartin.fr/"
                         ),
              stic=list(
                        name="Stichelton ",
                        address="Cuckney, England",
                        description="from <a href='https://en.wikipedia.org/wiki/Stichelton'>wikipedia</a>: Stichelton is an English blue cheese. It is similar to Blue Stilton cheese, except that it does not use pasteurised milk or factory-produced rennet. The name comes from a form of the name of Stilton village in the 1086 Domesday Book (Stichiltone/Sticiltone), as the name Stilton cannot legally be used for the cheese. ",
                        image="stichelton.jpg",
                        website="Farm site"
                        ),
              finot=list(
                         name="Domaine Finot",
                         address= "Bernin, Isère",
                         product="Tracteur Rouge, Red Wine",
                         description="It is while he was on his way back from Burgundy to Crozes-Hermitage that Thomas Finot went through the Grésivaudan Valley. He immediately fell in love with the region and; for about a dozen years now, has been working with the native grape varieties of these mountains. His estate has been one of the driving force behind the resurgence of this wine region which native grapes had not survived the <a href='https://en.wikipedia.org/wiki/Phylloxera'>Phylloxera</a>.

                         The 'Tracteur' cuvée is a blend of three varieties of red grapes natives from Savoie and its surroundings: <a href='https://en.wikipedia.org/wiki/Pinot_noir'>Pinot Noir</a>, <a href=https://en.wikipedia.org/wiki/Gamay´ >Gamay</a>, and <a href='https://en.wikipedia.org/wiki/Persan_%28grape%29'>Persan</a>. It's an easy-going and supple wine, bursting with fruit flavors. Very versatile!",
                         image="finot.jpg",
                         website="https://domaine-finot.com/"
                         ),
              d13L=list(
                        name="Domaine des 13 Lunes",
                        address="Mont Granier, Savoie",
                        product="Abymes, White Wine",
                        description="Trained as a carpenter and having transitioned through barrel refurbishing as a logical step, Sylvain Liotard grooms, on the gravel slopes of Mont Granier in Savoie, the local grapes that gives this region its characters and identity. 

                        The cuvée Abymes is 100% <a href='https://en.wikipedia.org/wiki/Jacqu%C3%A8re'>Jacquère</a> ; a vivid and fresh variety of grapes emblematic from the region; that yeld light and pure wines.",
                        image="abymes.jpg",
                        website="http://www.domainedes13lunes.com/"
                        ),
              pgr=list(
                       name="Paul Gadenne",
                       address="Chignin, Savoie",
                       product="Hors Norme, Red Wine",
                       description="Paul is young and recently establish winemaker in Chignin. He is part of this rising new generation of Savoyard winemakers. All the work is done by hand, from vine pruning to grape harvesting and native yeasts are used for fermentation. Well surrounded by some of the best winemakers in the area, his wines are precise and very accessible.

                       The cuvée 'Hors Norme rouge' comes from a semi-carbonic vinification of <a href='https://en.wikipedia.org/wiki/Mondeuse_noire'>Mondeuse</a>. This variety of red grape is emblematic of the region and produces a fresh and fruity wine with silky tannins.",
                       image="paulgadenne.jpg",
                       website="https://www.instagram.com/domaine_paul_gadenne"
                       ),
              giac=list(
                        name="Domaine Giachino",
                        address= "Savoie",
                        product= "Monfarina, White Wine",
                        description="Not only did they pioneer the production of high-end wines in Savoie, but the advice and support from Giachino Estate contributed to the accomplishment of a whole new generation of winemakers now flourishing all over the region. Their wines are incredibly pure and precise.</br>
                        The Monfarina cuvée is a blend of three indigenous grape varieties from Savoie: <a href='https://en.wikipedia.org/wiki/Jacqu%C3%A8re'>Jacquère</a>, <a href='https://en.wikipedia.org/wiki/Mondeuse_blanche'>Mondeuse blanche</a>, and <a href='https://en.wikipedia.org/wiki/Verdesse'>Verdesse</a>. It's a delightful cuvée, both fresh and indulgent, with a lovely aromatic intensity.",
                        image="monfarina.jpg",
                        website="https://domaine-giachino.fr/"
                        ),
              #bbc=list(
              #          name="Baron Bigod",
              #          address= "Fen Farm Suffolk",
              #          product="Baron Bigod, Cheese",
              #          description="Baron Bigod is a renowned British cheese, specifically a soft, bloomy-rind, unpasteurized cow's milk cheese. It's produced by Fen Farm Dairy in Suffolk, England. This artisanal cheese has gained recognition for its creamy texture and delicate, earthy flavors. What makes it unique is that it's made with raw milk, which contributes to its distinctive taste and character. Baron Bigod is often compared to French Brie",
              #          image="baron.jpg",
              #          website=""
              #),
              caer=list(
                        name="Gorwydd Caerphilly",
                        address= "Trethowan brothers farm in Somerset, UK.",
                        product="Gorwydd Caerphilly, Cheese",
                        description="Gorwydd Caerphilly is a traditional Welsh cheese, now crafted in Somerset, England, known for its crumbly texture, tangy lemony flavor, and natural edible rind. Made with unpasteurized cow's milk using age-old methods, it has received acclaim for its artisanal craftsmanship and versatility, making it a prized addition to cheeseboards and culinary creations.",
                        #from <a href='https://en.wikipedia.org/wiki/Caerphilly_cheese'>wikipiedia</a>:Caerphilly is a hard, crumbly white cheese that originated in the area around the town of Caerphilly, Wales. It is thought to have been created to provide food for the local coal miners. The Caerphilly of that period had a greater moisture content, and was made in local farms. At the start of the 20th century, competition for milk in the local area saw production decline, and Caerphilly production was gradually relocated to England. ",
                        image="caer.png",
                        website=""
                        ),
              anch=list(
                        name="Anchovies",
                        address= "Cantabria Spain",
                        product="Anchovies",
                        description="",
                        image="",
                        website=""
                        ),
              coc=list(
                       name="Cockle",
                       address= "Bocairent, Valencia",
                       product="Cockle",
                       description="",
                       image="",
                       website=""
                       ),
              ham=list(
                       name="Iberico Ham",
                       address= "Salamanca Spain",
                       product="Iberico Ham",
                       description="",
                       image="",
                       website=""
                       ),
              cgw=list(
                       name="Court Garden",
                       address= "England",
                       product="Court Garden, Sparkling White Wine",
                       description=" Court Garden is an  English vineyard and winery located in Sussex, known for its high-quality sparkling white wines. They specialize in traditional Champagne grape varieties and utilize the traditional method of wine production. Situated in the South Downs with chalky soils and a cool climate, Court Garden has received awards for its English sparkling wines, contributing to the reputation of English sparkling wine production. ",
                       image="cgw.jpg",
                       website="https://courtgarden.com"
                       ),
              azh=list(
                       name="Azahar",
                       address= "Market Square, Cambridge",
                       product="Spanish products",
                       description="A small independent Spanish and English family business specialising in importing the finest products from across Spain.",
                       image="azh.jpg",
                       website="http://www.azaharartisanspanishfood.com/"
                       ),
              apj=list(
                       name="Warden House",
                       address= "322 Market Hill, Cambridge",
                       product="Apple Juice",
                       description="Pure apple, with a tiny bit of lemon juice to avoid the color to change! Directly from their Garden to the market and for you to try! ",
                       image="apj.jpg",
                       website=""
                       ),
              mcdo=list(
                        name="McDonald Institute for Archaeological Research",
                        address= "Cambridge, England",
                        product="Nothing",
                        description="The McDonald Institute for Archaeological Research provides a shared intellectual home for archaeologists at Cambridge and their collaborators into all aspects of the human past, across time and space.",
                        image="mcdo.jpg",
                        website="https://www.mcdonald.cam.ac.uk"
                        ),
              prs=list(
                       name="Paris",
                       address= "Paris France",
                       product="Nothing",
                       description="Simon bringing back cheese and wine from France using French 'TGV' and the Eurostar",
                       image="paris.jpg",
                       website=""
                       ),
              haf=list(
                       name="Hafod Cheddar",
                       address= "Lampeter,Wales",
                       product="Hafod Cheddar",
                       description="Hafod Cheddar is an artisanal cheese produced in Wales at the Hafod cheese dairy, known for its commitment to sustainable and organic farming practices. Made from raw Ayrshire cow's milk, this farmstead cheese is aged for at least 16 months, resulting in a creamy and nutty flavor with hints of grass and herbs. Hafod Cheddar has received numerous awards, representing a fine example of traditional British cheddar, while its dedication to sustainability and environmental stewardship underscores its artisanal quality and commitment to preserving traditional cheese-making methods.",
                       image="hafod.jpg",
                       website=""
                       ),
              echansson=list(
                             name="L'Échansson Crolles",
                             address="Crolles, Isère",
                             description="L'Échansson is a familly run buisness in the City of Crolles ; that sells variety of products, mainly wines but also whiskies, rhums, beers, cheeses and others.",
                             image="Echansson.jpg",
                             website=""
                             ),
              fred=list(
                        name="Frederic's Cambridge",
                        address= "Magdalene Street, near Magdalen's Bridge, Cambridge",
                        product="Artisan, Food & Wine",
                        description="A small shop near Magdalen's Bridge ",
                        image="fred.jpg",
                        website="https://www.instagram.com/fredericscambridge/"
                        ),
              atv=list(
                       name="À Tous Vins",
                       address= "Le Touvet",
                       description="They are sommeliers, merchants and wine enthousiasts who love high quality wines that bring you to travel in space and time. In the small city of Le Touvet, At the feet of the cliff of the <a href='https://en.wikipedia.org/wiki/Chartreuse_Mountains'>Chartreuse</a> mountain range, their shop aims at introducing small producers and to share with everyone the hard work, efforts and stories it takes to make a good bottle of wine. 
                       </br>
                       All the wines coming from their place  are produced by winemakers who share a common philosophy regarding the growth and development of local wines and native grape varieties of the region. They are committed to responsible viticulture, minimizing inputs both in the vineyard and in the cellar.
                       </br>
                       These are winemakers with whom À Tout Vins team  have been working from the very beginning -- even since their first vintage, for Sylvain from Domaine des 13 Lunes, Paul, and Thomas; sharing with all of them the ambition to showcase the beauty and quality of the local terroir.  The Giachino's brothers, pionners in the region, have helped the other three by sharig their methods and thechniques.",
                       image="atv.jpg",
                       website="https://www.atousvins.fr/")
)
allcolab=lapply(allcolab,function(i){if(is.null(i$product))i$product="";return(i)})
#saveRDS(allcolab,file="allprod.RDS")
			   

city_data=do.call(rbind.data.frame,allcolab)
city_data=cbind.data.frame(id=rownames(city_data),city_data)


allcoords=readRDS(file="allcoords.RDS")
if(sum(!(names(allcolab) %in% names(allcoords)))!=0){
    print("problem")
    for(n in names(allcolab))
        allcoords[[n]]=tryCatch(geocode(paste(allcolab[[n]]$name,allcolab[[n]]$address)),error=function(n)NULL)
    allcoords=lapply(paste(city_data$name,city_data$address),geocode)
	names(allcoords)=city_data$id
    #saveRDS(file="allcoords.RDS",allcoords)
}


allcoords=allcoords[names(allcolab)]

city_data$lng = sapply(allcoords,"[[","lon")
city_data$lat = sapply(allcoords,"[[","lat")

commu=list(atv = c("d13L", "pgr","finot","giac"),
		   fred = c("stic","caer","cgw","bbc","haf","ogle"),
		   echansson = c("comte"),
		   azh = c("ham","coc","anch"),
		   mcd=c("pastim","fred","azh"),
		   prs=c("echansson","atv","mcdo"),
		   mcdo=c("prs","azh","fred","apj","past","stir")
           )
#comcol=palette.colors(length(commu),"Tableau")
comcol=c("#E41A1C","#377EB8","#4DAF4A","#984EA3","#FF7F00","#FFFF33")
names(comcol)=names(commu)
city_data$color="green"
city_data$radius=4
for(i in names(commu)){
    city_data$radius[city_data$id==i]=6
    city_data$color[city_data$id==i]=comcol[i]
}


# Define server logic
server <- function(input, output, session) {
	#
	currentlySelectedCity <- reactiveVal(NULL)

	output$mymap <- renderLeaflet({

		query <- parseQueryString(session$clientData$url_search)

		if ("city" %in% names(query) && query$city %in% city_data$id) {
			selected_city <- query$city
			lat_center <- city_data[city_data$id == selected_city, "lat"]
			lng_center <- city_data[city_data$id == selected_city, "lng"]
			zoom_level <- 10
			currentlySelectedCity(selected_city)
		} else {
			# If no city specified, set some default values
			selected_city=""
			lat_center <- mean(range(city_data$lat))  # Default lat (could be the middle of your city list)
			lng_center <- mean(range(city_data$lng))  # Default lat (could be the middle of your city list)
			zoom_level <- 6
		}

		utm_source <- query[['utm_source']]
		message(paste0(Sys.time()," connect from: '",utm_source,"' looking up '",selected_city,"'"))
		# Initialize map
		m <- leaflet(city_data) 
		m <- addProviderTiles(m,providers$OpenStreetMap) 
		m <- setView(m,lng = lng_center, lat = lat_center, zoom = zoom_level)
		for(cn in names(commu)){
			for( i in commu[[cn]]){
				layerid=paste0(cn,i)
				m<-addPolylines(m,
								lng = city_data$lng[city_data$id %in% c(cn,i)],
								lat = city_data$lat[city_data$id %in% c(cn,i)],
								color =as.character(comcol[cn]),
								weight = 5,
								opacity =.3,
								layerId = layerid
				)
			}
		}

		# Add markers
		m <- addCircleMarkers(m,
							  lng = ~lng, lat = ~lat,
							  popup = ~paste0("<b>", name, '</b>'
#<div style="overflow-y:auto; max-height:50px;max-width:80px;">', description,"</div></br>"
							                  ),
							  #popup = ~paste0("<b>", name, '</b><br><em>location:</em>',address,"</br>"),
							  color = ~color,
							  radius = ~radius,
							  fillOpacity = .9,
							  opacity = .9,
							  layerId = ~id
		)
		# Open popup automatically based on URL
		m <- onRender(m,paste0("
							   function(el, x) {
								   var chosenLayer;
								   this.eachLayer(function(layer) {
													  if (layer.options.layerId === '", selected_city, "') {
														  chosenLayer = layer;
													  }
});
								   if (chosenLayer) {
									   chosenLayer.openPopup();
								   }
							   }
							   "))
	})

		observeEvent(input$mymap_marker_click,{
						 click = input$mymap_marker_click
						 clickedCity <- click$id # The 'id' here is the 'layerId' we set while adding markers, which is the city name
						 currentlySelectedCity(clickedCity)
	}, ignoreInit = TRUE)

			output$infos <- renderUI({
				clickedCity <- currentlySelectedCity()
				if(!is.null(clickedCity)){
                    print(clickedCity)
					cityData <- city_data[city_data$id == clickedCity, ]
                    message(paste0(Sys.time()," clickin on: '",clickedCity,"'"))
                    HTML(paste0("<b>", cityData$name, '</b><br><div style="overflow-y:auto; max-height:50vh;">', cityData$description,"</div></br>",
                                '<em>location:</em> ',cityData$address,"</br>",
                                ifelse(!(cityData$website == ""),paste0("<em>more </em><a href='",cityData$website,"'>online</a>"),"</br>"),
                                "</br>",
                                "</div><br><a href='",cityData$image,"'><img src='", cityData$image, "' width='90%'></a>"))
                }
                else{
                    HTML("click on a point on the map to see details")
                }
			})


}

# Run the application 
shinyApp(ui = ui, server = server)



