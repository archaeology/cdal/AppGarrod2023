library(qrencoder)
allprod=readRDS("allprod.RDS")
basurl='https://garrod.shinyapps.io/appgarrod2023/' 
basurl='https://theia.arch.cam.ac.uk/garrod/'
for( n in names(allprod)){
    url=paste0(basurl, "?city=",n,"&utm_source=qr")
    paste("qrencode",paste0(url," -s 50 -l L -o ",n,".png"))
    #system(paste("qrencode",paste0(url," -s 50 -l L -o ",n,".png")))
    pdf(paste0("qr_",n,".pdf"),height=12,width=7)
    par(mar=c(0,5,10,5),xpd=T)
    image(qrencode_raster(url), 
          asp=1, col=c("white", "black"), axes=FALSE, 
          xlab="", ylab="")
    prd=allprod[[n]]$product
    if(is.null(prd))prd=allprod[[n]]$name
    mtext(gsub(",","\n",prd),3,1,cex=4)
    dev.off()
}

